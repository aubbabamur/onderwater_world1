
const infoBox = document.getElementById('battleInfo');
const heroHp = document.getElementById('heroHp');
const dragonHp = document.getElementById('dragonHp');

const INIT_DRAGON_HP = 2000;
const INIT_HERO_HP = 1000;

heroHp.innerText = String(INIT_HERO_HP + ' / ' + INIT_HERO_HP);
dragonHp.innerText = String(INIT_DRAGON_HP + ' / ' + INIT_DRAGON_HP);


const Dragon = {
  hp: INIT_DRAGON_HP,
  defense: 120,
  str: 250,
  weapon: 0,
  toString: function () {
    return 'Дракон: ' + this.hp + ' HP';
  },
  modifyHealth: function (hitPoints) {
    this.hp += hitPoints;
    if (this.hp < 0) {
      this.hp = 0;
    }
  },
};

const Hero = {
  hp: INIT_HERO_HP,
  defense: 100,
  str: 120,
  weapon: 250,
  shield: 150,
  isShieldEquipped: false,
  equipShield: function () {
    this.isShieldEquipped = true;
  },
  removeShield: function () {
    this.isShieldEquipped = false;
  },
  toString: function () {
    return 'Герой: ' + this.hp + ' HP';
  },
  modifyHealth: function (hitPoints) {
    this.hp += hitPoints;
    if (this.hp < 0) {
      this.hp = 0;
    }
  },
};

const chanceOfHit = function () {
  return Math.floor(Math.random() * 100);
};

const rangeOfDamage = function (damage) {
  return damage + Math.floor(Math.random() * 21 - 10);
}

const hitHero = function () {
  let hitPoints = 0;
  if (chanceOfHit() < 75) {
    hitPoints = -(Hero.str + Hero.weapon - Dragon.defense);
    hitPoints = rangeOfDamage(hitPoints);
    if (hitPoints > 0) {
      hitPoints = null;
    }
  }
  return hitPoints;
};

const hitDragon = function () {
  let hitPoints = 0;
  if (chanceOfHit() < 50) {
    hitPoints = -(Dragon.str + Dragon.weapon - Hero.defense);
    if (Hero.isShieldEquipped) {
      hitPoints += Hero.shield;
    }
    hitPoints = rangeOfDamage(hitPoints);
    if (hitPoints > 0) {
      hitPoints = null;
    }
  }
  return hitPoints;
};

const addMessage = (text) => {
  const msgBox = document.createElement('p');
  msgBox.innerText = text;
  infoBox.append(msgBox);
}

const dragonHit = () => {
  const hitPoints = hitDragon();
  Hero.modifyHealth(hitPoints);
  if (hitPoints < 0) {
    const message = ('Дракон нанес ' + (-hitPoints) + ' единиц урона\n');
    if (Hero.hp === 0) {
      addMessage(message + 'Дракон одержал победу!');
      addMessage('====================');
      addMessage(Hero.toString() + '\n' + Dragon.toString());
    } else {
      addMessage(message + 'У героя осталось ' + Hero.hp + ' HP');
    }
  } else if (hitPoints === null) {
    addMessage('Дракон не пробил защиту\n' + Hero.toString());
  } else {
    addMessage('Дракон проспал\n' + Hero.toString());
  }

  Hero.removeShield();

  
  heroHp.style.width = (Hero.hp / INIT_HERO_HP * 100) + '%';
  dragonHp.style.width = (Dragon.hp / INIT_DRAGON_HP * 100) + '%';
  heroHp.innerText = INIT_HERO_HP + ' / ' + Hero.hp;
  dragonHp.innerText = Dragon.hp + ' / ' + INIT_DRAGON_HP;
}

addMessage(Hero.toString() + '\n' + Dragon.toString());
addMessage('====================');



const attackBtn = document.getElementById('attack');
const defenseBtn = document.getElementById('defense');
const nothingBtn = document.getElementById('nothing');

attackBtn.addEventListener('click', () => {
  const hitPoints = hitHero();
  Dragon.modifyHealth(hitPoints);
  if (hitPoints < 0) {
    const message = 'Герой нанес ' + (-hitPoints) + ' единиц урона\n';
    if (Dragon.hp === 0) {
      addMessage(message + 'Герой одержал победу!');
      addMessage('====================');
      addMessage(Hero.toString() + '\n' + Dragon.toString());
    } else {
      addMessage(message + 'У дракона осталось ' + Dragon.hp + ' HP');
    }
  } else if (hitPoints === null) {
    addMessage('Герой не пробил защиту\n' + Dragon.toString());
  } else {
    addMessage('Герой промахнулся\n' + Dragon.toString());
  }
  dragonHit();
});

defenseBtn.addEventListener('click', () => {
  addMessage('Герой перешел к обороне. Защита героя увеличена');
  Hero.equipShield();
  dragonHit();
});

nothingBtn.addEventListener('click', () => {
  addMessage('Герой не предпринял никаких действий');
  dragonHit();
});


